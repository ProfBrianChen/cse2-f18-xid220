//lab08
//xid220
//Xinyi Deng
//cse002
//Objectives. this lab session will get you familiar with one-dimensional arrays.
import java. util. Arrays;
import java.util.HashMap;
import java.util.Map;

public class lab08
{
	public static void main(String[] args)
	{
	int [] array1 = new int[100];
	int [] array2 = new int[100];
	System.out.println("Array 1 holds the following integers ");
	for(int i = 0; i < 100; i++)
	{
		int rand = (int)(Math.random()*100);
		array1[i] = rand;
		System.out.print(array1[i]);
		System.out.print(" ");
	}
	//int [] array1 =  {3, 1, 7, 5,9, 0, 1, 9 ,2, 9}; for testing the sample input 
	Arrays. sort(array1);
	 HashMap<Integer, Integer> elementCountMap = new HashMap<Integer, Integer>();
     //using hash map to count the occurrence of numbers
	    Map<Integer, Integer> map = new HashMap<>();
        for (int key : array1) {
            if (map.containsKey(key)) {
                int occurrence = map.get(key);
                occurrence++;
                map.put(key, occurrence);
            } else {
                map.put(key, 1);
            }
        }

        for (Integer key : map.keySet()) {
            int occurrence = map.get(key);
           for(int i = 0; i < 100; i++)
           {
        	 array2[i] =  occurrence;
           }
            System.out.println(key + " occur " + occurrence + " time(s).");
        }
	}
	
}
