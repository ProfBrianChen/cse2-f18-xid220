import java.util.Random;
//importing the scanner utility
import java.util.Scanner;

public class Method{
    //adding main method
    public static void main(String[] args){
        //declaring myScan for user input
        Scanner myScan = new Scanner(System.in);
        int x = 0;
        System.out.print("Generate a sentence, y/n: ");
        while (x == 0){
            while(!myScan.hasNext()){
                System.out.print("Invalid input, respond with y/n: ");
                myScan.next();
            }
            String y = myScan.nextLine();//string y contains user input
            y = y.toLowerCase();
            switch(y){
                case "y":
            String k = Adjectives();
            String l = Subject();
            String g = Verb();
            String o = Obj();
            System.out.println("The "+k+ " "+l+" "+g+" the "+o+". ");//generating sentence
            System.out.print("Would you like to generate new? y/n: ");//asking user for input regarding another sentence
            while(!myScan.hasNext()){//loop forcing user to input a string
                System.out.println("Invalid input, respond with y/n: ");
                myScan.next();
            }
            break;
                case "n"://if user input no, x becomes 2 and java exits the loop
                    x = 2; 
                    break;
            }
        
        }
    System.out.println(Final());//calling the final method and printing it
}
//adding the method adjectives
    public static String Adjectives(){
        Random randomGenerator= new Random();//declaring randomgenerator to generate numbers
        int randomInt = randomGenerator.nextInt(10);//declaring randomInt as int generated
        String a = "";//empty string for later user
        switch(randomInt){//switch based on generated number to pick one of the words below
            case 0:
                a = "adorable";
                break;
            case 1:
                a = "beautiful";
                break;
            case 2: 
                a = "clean";
                break;
            case 3:
                a = "drab";
                break;
            case 4:
                a = "elegant";
                break;
            case 5:
                a = "glamorous";
                break;
            case 6:
                a = "quaint";
                break;
            case 7:
                a = "sparkling";
                break;
            case 8:
                a = "unsightly";
                break;
            case 9:
                a = "agreeable";
                break;
        }
        return a;//return the word selected
    }//end of method
    //adding method Subject
    public static String Subject(){
        Random randomGenerator= new Random();//randomGenerator 
        int randomInt = randomGenerator.nextInt(10);//randomInt for number generated
        String b = "";//empty string
        switch(randomInt){//switch to word based on generated number
            case 0:
                b = "firemen";
                break;
            case 1:
                b = "boys";
                break;
            case 2:
                b = "girls";
                break;
            case 3:
                b = "teachers";
                break;
            case 4:
                b = "parents";
                break;
            case 5:
                b = "pencils";
                break;
            case 6:
                b = "backpacks";
                break;
            case 7:
                b = "imagination";
                break;
            case 8:
                b = "imagination";
                break;
            case 9:
                b = " courage";
                break;
        }
        return b;
    }
    public static String Verb(){
        Random randomGenerator= new Random();//random number generator
        int randomInt = randomGenerator.nextInt(10);//randomInt: number generated
        String c = "";//empty string
        switch(randomInt){//switch choose word based on random number
            case 0:
                c = "beam";
                break;
            case 1:
                c = "bellow";
                break;
            case 2:
                c = "blink";
                break;
            case 3: 
                c = "correct";
                break;
            case 4:
                c = "dress";
                break;
            case 5: 
                c = "film";
                break;
            case 6:
                c = "harass";
                break;
            case 7:
                c = "hoot";
                break;
            case 8:
                c = "infect";
                break;
            case 9:
                c = "joke";
                break;
        }
        return c;//return word
    }
    public static String Obj(){
        Random randomGenerator= new Random();//random number generator
        int randomInt = randomGenerator.nextInt(10);//randomInt: number generated
        String d = "";//empty string
        switch(randomInt){
            case 0: 
                d = "cite";
                break;
            case 1:
                d = "match";
                break;
            case 2:
                d = "schedule";
                break;
            case 3:
                d = "question";
                break;
            case 4:
                d = "distinguish";
                break;
            case 5:
                d = "develop";
                break;
            case 6:
                d = "criticize";
                break;
            case 7:
                d = "select";
                break;
            case 8:
                d = "select";
                break;
            case 9:
                d = "road";
        }
        return d;//return word chosen
    }
    
    public static String Thesis(){
        String k = Adjectives();
        String l = Subject();
        String g = Verb();
        String o = Obj();
        System.out.print("The "+k+ " "+l+" "+g+" the "+o+". ");//print out the thesis line
        return l;//return the subject
    }
    
    public static String body(){
        Random randomGenerator= new Random();//random number generator
        int randomInt = randomGenerator.nextInt(10);//randomInt number generated
        String k = Adjectives();
        String g = Verb();
        String o = Obj();
        String sub;
        switch(randomInt%2){
            case 0://if even use "it"
                sub = "it";
                break;
            default:
                sub = "The "+Thesis();
                break;
        }
        String bod = sub+" "+g+" the "+k+" "+o+". ";
        return bod;
    }
    public static String conclusion(){
         Random randomGenerator= new Random();//random number generator
        int randomInt = randomGenerator.nextInt(10);//randomInt number generated
        String k = Adjectives();
        String g = Verb();
        String o = Obj();
        String subj;
        switch(randomInt%2){//switch statemtent for randomization of it and subject
            case 0:
                subj = "it";//if even use it
                break;
            default:
                subj = "The "+Thesis();//if odd use thesis
                break;
        }
        String con = subj+" "+g+" the "+o+". ";
        return con;
        
    }
    public static String Final(){
        String con="";//empty string to represent the conclusion
        Random randomGenerator= new Random();//random number generator
        int randomInt = randomGenerator.nextInt(20);//number generated
        //for loop which create random sentences when i is less than number generated
        for(int i=0; i<randomInt; i++){
        String body = body();//calling body method
        System.out.print(body);//printing  string body
        }
        con = conclusion();//calling method
        return con;//return conclusion
    }
    
}