//xid220
//Xinyi Deng
// hw08 
//practice string array manipulation
import java.util.Scanner;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards);
System.out.println();
System.out.println("Shuffled");
shuffle(cards); 
printArray(cards);
System.out.println();
while(again == 1){ 
	System.out.println("Hands");
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   System.out.println();
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
private static void printArray(String[] array )
{
	int n = array.length;
	for(int i = 0; i < n ; i++)
	{
		System.out.print(array[i] + " ");
	}
}

private static void shuffle(String[] s){
   // loop through 52 times 
	for(int i=0;i<52;i++)
	{
     int rand=(int)(Math.random()*51)+1; // generate random number 
 	   String temp=s[0]; //store the first elements
		s[0]=s[rand]; //swap the element
		s[rand]=temp;
	}
}
private static String[] getHand(String[]list, int index, int numCards)
{ 
	String[] result = new String[numCards];
	for(int i = 0; i < numCards;i++)
	{
		result[i] = list[index - i];
	}
	return result;
}
}
