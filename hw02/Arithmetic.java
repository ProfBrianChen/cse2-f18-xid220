
// CSE2 homework2
//xinyi Deng
// use finction for calculation
//xid220
import java.text.DecimalFormat;
   private static DecimalFormat df2 = new DecimalFormat(".##");
  public static void mainpublic class Arithmetic {
(String args[]){
 //Number of pairs of pants
   int numPants = 3;

//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;

//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;

//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;


double totalPants = numPants * pantsPrice;
double totalShirt = numShirts * shirtPrice;
double totalBelts = numBelts * beltCost;
double shirtsTax = paSalesTax * totalShirt;
double pantsTax = paSalesTax * totalPants;
double beltsTax = paSalesTax * totalBelts;
double totalCostPants = numPants * pantsPrice + pantsTax;
double totalCostShirt = numShirts * shirtPrice + shirtsTax;
double totalCostBelts = numBelts * beltCost + beltsTax;
double totalCost = totalCostPants + totalCostShirt + totalCostBelts;
double totalTax = shirtsTax + pantsTax + beltsTax;
double totalPaid = totalCost + totalTax;

System.out.println("The total cost of pants is " + df2.format(totalCostPants));
System.out.println("The total cost of shirt is " + df2.format(totalCostShirt));
System.out.println("The total cost of belts is " + df2.format(totalCostBelts));
System.out.println("The total tax of pants is " +  df2.format(pantsTax));
System.out.println("The total tax of belts is " + df2.format(beltsTax));
System.out.println("The total tax of shirts is " + df2.format(shirtsTax));
System.out.println("The total purchase of Pants is " + df2.format(totalPants));
System.out.println("The total purchase of Shirt is " + df2.format(totalShirt));
System.out.println("The total purchase of Belts is " + df2.format(totalBelts));
System.out.println("The total purchase for purchase before tax is " + df2.format(totalCost));
System.out.println("The total tax is " + df2.format(totalTax));
System.out.println("The total paid for transaction(including tax) is " + df2.format(totalPaid));
}
}