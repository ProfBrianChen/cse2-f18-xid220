//hw05
//This is a program that count the cases of pocker hand
//author: Xinyi Deng 
//xid220
import java.util.Scanner;
import java.text.DecimalFormat;
public class Pocker {
	public static void main(String[] args) {
		int num = 0 ;
		int current = 0;
		int countFour = 0;
		int countThree = 0;
		int countTwoPair = 0;
		int countOnePair = 0;
		int random1 = 0;
		int random2 = 0;
		int random3 = 0;
		int random4 = 0;
		int random5 = 0;
		System.out.println("please enter the number of times user want to generate hands");
		Scanner myScanner = new Scanner( System.in );
		while(!myScanner.hasNextInt()) 
		{
			System.out.println("please enter an integer");
			myScanner.next();
		}
		num = myScanner.nextInt();
		System.out.println("The number of loops : (user inputted integer)" + num);
		while (current < num)
		{//Check randoom number is unique 
		   random1 = (int)(Math.random()*(52+1)) + 1;
		   random2 = (int)(Math.random()*(52+1)) + 1;
		while(random1 == random2)
		{
		    random2 = (int)(Math.random()*(52+1)) + 1;
		}
		  random3 = (int)(Math.random()*(52+1)) + 1;
	    while((random2 == random3) || (random1 == random3))
		 {
			 random3 = (int)(Math.random()*(52+1)) + 1;
		 }
		   random4 = (int)(Math.random()*(52+1)) + 1;
        while((random1 == random4)||(random2 == random4) || (random3 == random4))
		  {
			  random4 = (int)(Math.random()*(52+1)) + 1;
		  }
		   random5 = (int)(Math.random()*(52+1)) + 1;
	    while((random1 == random5)||(random2 == random5) || (random3 == random5)|| (random4 == random5))
		  {
			  random5 = (int)(Math.random()*(52+1)) + 1;
		  }
		int ran1 = random1 % 13 + 1;
		int ran2 = random2 % 13 + 1;
		int ran3 = random3 % 13 + 1;
		int ran4 = random4 % 13 + 1;
		int ran5 = random5 % 13 + 1;
		int Card[] = new int[5];
		Card[0] = ran1;
		Card[1] = ran2;
		Card[2] = ran3;
		Card[3] = ran4;
		Card[4] = ran5;
		int n = Card.length;
        for (int i = 0; i < n; i++) {
        	Card[i]--;
        }
 
        for (int i = 0; i < n; i++) {
        	Card[Card[i] % n] += n;
        }
        //count the number of cards occrurs 4 time
        for (int i = 0; i < n; i++) {
        	if(Card[i] / n == 4)
        	{
        		countFour++;
        	}
        }
        for (int i = 0; i < n; i++) {
        	if(Card[i] / n == 3)
        	{
        		 countThree++;
        	}
        }
        int count3 = 0;
        for (int i = 0; i < n; i++) {
        	
        	if(Card[i] / n == 2)
        	{
        		count3++;
        	}
        }
        if(count3 == 2)
        {
        	countTwoPair++;
        }
        if(count3 == 1)
        {
        	countOnePair++;
        }
        current++;
		}
		
		double posibilityFour = (countFour *1.00 )/ num;
		double posibilityThree = (countThree *1.00) / num;
		double posibilityTwo = (countTwoPair *1.00) / num;
		double posibilityOne = (countOnePair *1.00) / num;
		 DecimalFormat df = new DecimalFormat("0.000");
	    String posibilityFour1 = df.format(posibilityFour);
	    String posibilityThree1 = df.format( posibilityThree);
	    String posibilityTwo1 = df.format( posibilityTwo);
	    String posibilityOne1 = df.format( posibilityOne);
		System.out.println("The probability of Four-of-a-kind:" + posibilityFour1);
		System.out.println("The probability of Three-of-a-kind: " + posibilityThree1);
		System.out.println("The probability of Two-pair: " + posibilityTwo1);
		System.out.println("The probability of One-pair: " + posibilityOne1);
		
	}
}




