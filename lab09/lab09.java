//lab 09
//xid220
//xinyi Deng

public class lab09 {
	public static void main(String[] args) {
  int [] array0 = {1,2,3,4,5,6,7,8,9};
  int [] array1  = copy(array0);
  int [] array2 = copy(array0);
  inverter(array0);
  print(array0);
  array1 = inverter2(array1);
  print(array1);
  int array3[] = inverter2(array2);
  print(array3);
}   // take in array make a copy of it and then return 
	private static int[] copy(int[] array)
	{ 
		int n = array.length;
		int [] copy = new int [n];
	    for(int i = 0; i < n; i++)
	    {
	    	copy[i] = array[i];
	    }
	    return copy;
	    
	}
	//reverse the array and return void 
    private static void inverter(int[] array)
    {
    	int n = array.length;
        for(int i = 0; i < n/2 ; i++)
        {
           int temp = array[i];
           array[i] = array[n - i - 1];
           array[n - i - 1] = temp;
        } 
    }
    //take in array and return the reversed array
    private static int[] inverter2(int[] array)
    {
    	int [] copy = copy(array);
    	int n = copy.length;
        for(int i = 0; i < n/2 ; i++)
        {
           int temp = copy[i];
           copy[i] = copy[n - i - 1];
           copy[n - i - 1] = temp;
        }
        return copy;
    }
    //print method to print out the array
    public static void print(int[] array)
    {
    	for(int i = 0 ; i < array.length; i++)
    	{
    		System.out.print(array[i] + " ");
    	}
    	System.out.println();
    }
	
}
