//CSE 2 
//This program use scanner  obtain from the user the original
//cost of the check, the percentage tip they wish to pay, 
//and the number of ways the check will be split.
//Then determine how much each person in the group needs to spend in order to pay the check
//xid220
//Xinyi Deng 
import java.util.Scanner;
	public class Check{
						// main method required for every Java program
					public static void main(String[] args) {
//create new acanner
	Scanner myScanner = new Scanner( System.in );
// prompt the user to enter the original cost
	System.out.print("Enter the original cost of the check in the form xx.xx: ");
	// user another scanner to input checkCost
	double checkCost = myScanner.nextDouble();
	// prompt the user to enter percentage tip
		System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
	// scanner for tippercent
	double tipPercent = myScanner.nextDouble();
	tipPercent /= 100;
	System.out.print("Enter the number of people who went out to dinner: ");
	// scanner to input numPeople
	int numPeople = myScanner.nextInt();
	double totalCost;
	double costPerPerson;
	int dollars,   //whole dollar amount of cost 
				dimes, pennies; //for storing digits
						//to the right of the decimal point 
						//for the cost$ 
	totalCost = checkCost * (1 + tipPercent);
	costPerPerson = totalCost / numPeople;
	//get the whole amount, dropping decimal fraction
	dollars=(int)costPerPerson;
	//get dimes amount, e.g., 
	// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
	//  where the % (mod) operator returns the remainder
	//  after the division:   583%100 -> 83, 27%5 -> 2 
	dimes=(int)(costPerPerson * 10) % 10;
	pennies=(int)(costPerPerson * 100) % 10;
	// the output for bill
	System.out.println("Each person in the group owes $"+ dollars + '.' + dimes + pennies);


	}  //end of main method   
			} //end of class

