import java.util.Scanner;
//do the program use only switch statement
//cse2
//xinyi deng 
//hw04
public class CrapsSwitch {
    public static void main(String[] args) {
        int dice1 = 0;
        int dice2 = 0;
        System.out.println("If you want to random generate dice enter 1 else enter 2");
        Scanner scan = new Scanner(System.in);
        int result = scan.nextInt();
        switch (result) {
        case 1:
            int random1 = (int) (Math.random() * 6) + 1;
            int random2 = (int) (Math.random() * 6) + 1;
            System.out.println("Your random generated first dice is " + random1);
            System.out.println("Your random generated secccond dice is " + random2);
            dice1 = random1;
            dice2 = random2;
            break;
        case 2:
            System.out.println("Please enter the result for the first dice");
            Scanner scan1 = new Scanner(System.in);
            int result1 = scan1.nextInt();
						//when user enter integer that is outof bounds we ask them to enter again
            while ((result1 < 1) || (result1 > 6)) {
                System.out.println("Your input is not valid, please enter valid input (1-6)");
                Scanner scan3 = new Scanner(System.in);
                result1 = scan3.nextInt();
            }
            dice1 = result1;
            System.out.println("Please enter the result for the second dice");
            Scanner scan2 = new Scanner(System.in);
            int result2 = scan2.nextInt();
            while ((result2 < 1) || (result2 > 6)) {
                System.out.println("Your input is not valid, please enter valid input (1-6)");
                Scanner scan4 = new Scanner(System.in);
                result2 = scan4.nextInt();
            }
            dice2 = result2;
            break;
        default:
            System.out.println("Please enter 1 or 2(for choosing random gnerate or not), your input is not valid");
            break;
        }
        int total = dice1 + dice2;
//switch total number of the result of two dices
        switch (total) {
        case 2:
            System.out.println("Your Result is Snake Eyes");
            break;
        case 3:
            System.out.println("Your Result is Ace Deuce");
            break;
        case 4://nested swith statements to detact diffrences when total is the same but need to print different statements
            switch (dice1) {
            case 2:
                System.out.println("Your Result is Hard four");
                break;
            default:
                System.out.println("Your Result is Easy Four");
                break;
            }
            break;
        case 5:
            System.out.println("Your Result is Fever five");

            break;
        case 6:
            switch (dice1) {
            case 3:
                System.out.println("Your Result is Hard six");
                break;
            default:
                System.out.println("Your Result is Easy six");
                break;
            }
            break;
        case 7:
            System.out.println("Your Result is Seven out");
            break;
        case 8:
            switch (dice1) {
            case 4:
                System.out.println("Your Result is Hard Eight");
                break;
            default:
                System.out.println("Your Result is Easy Eight");
                break;
            }
            break;
        case 9:
            System.out.println("Your Result is Nine");
            break;
        case 10:
            switch (dice1) {
            case 5:
                System.out.println("Your Result is Hard Ten");
                break;
            default:
                System.out.println("Your Result is Easy Ten");
                break;
            }
            break;
        case 11:
            System.out.println("Your Result is Yo-leven");
            break;
        case 12:
            System.out.println("Your Result is Boxcars");
            break;

        }
    }
}
