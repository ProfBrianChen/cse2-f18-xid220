// CSE02 lab02
// Xinyi Deng xid220 Sept 6 2018
// Cyclometer program print the number of minutes countes for each trip and 
//print the distance of each trip in miles
//print the distance for the two trips combined
public class Cyclometer{
  public static void main(String[] args){
    // our input data. Document your variables by placing your

	  int secsTrip1=480;  // the secondsfor trip1
    int secsTrip2=3220;  //the secondsfor trip2
		int countsTrip1=1561;  // the counts for trip1
		int countsTrip2=9037; // the counts for trip2
    double wheelDiameter=27.0;  //wheelDiameter
  	double PI=3.14159;//Pi value
  	int feetPerMile=5280; //feetPerMile value
  	int inchesPerFoot=12;  //inchesPerFoot value
  	int secondsPerMinute=60;  // secondsPerMinute value
	  System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	   System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    double distanceTrip1, distanceTrip2,totalDistance;  
    //run the calculations; store the values. Document your
		//calculation here. 
	  distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; 
    // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
    //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");


  }
}