//hw06
//this program let the user to enter a number between 0 and 100 and then display the desired pattern
//xid220
//Xinyi Deng 
import java.util.Scanner;
	public class EncryptedX{
			public static void main(String[] args) {
		  int number = 0;
			System.out.println("Please enter an integer between 1 and 100");
			Scanner myScanner = new Scanner( System.in );
			//prompt the user to enter again if not enter a number
			while(!myScanner.hasNextInt())
			{
				System.out.println("You need to enter an integer, please enter again:");
				myScanner.next();
			}
			// prompt the user to enter again of the number if out of bound
			number = myScanner.nextInt();
			while((number < 0) || (number > 100))
			{
				System.out.println("The integer you entered is out of bound, please enter again:");
					number = myScanner.nextInt();
    	}
    	//display the desired pattern
			System.out.println("The number you entered is " + number);
			for (int i = 0; i < number; i++)
			{
				for(int j = 0; j < number; j++)
				{
					if((j ==i) || (j == number - i-1))
					{
						System.out.print(" ");
					}
					else{
							System.out.print("*");
					}
				
				}
				System.out.println();
			}
		}

			}
					
			
		

