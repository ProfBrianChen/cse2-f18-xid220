import java.util.Scanner;
	public class PatternA{
		public static void main(String[] args) {
		  int number = 0;
			System.out.println("Please enter an integer between 1 and 10");
			Scanner myScanner = new Scanner( System.in );
			while(!myScanner.hasNextInt())
			{
				System.out.println("You need to enter an integer, please enter again:");
				myScanner.next();
			}
			number = myScanner.nextInt();
			while((number < 1) || (number > 10))
			{
				System.out.println("The integer you entered is out of bound, please enter again:");
					number = myScanner.nextInt();
    	}
			System.out.println("The number you entered is " + number);
	    for (int i = 1; i <= number; i++){
				for(int j = 1; j < i; j++)
				{
					System.out.print(j + " ");
				}
				System.out.println(i);
			}
					
		}
 }
					 
					 
					