// CSE 2 
//This program ask the user enter the course number 
// the department  the number of times meet in a week and 
//the time the class starts , the name of the instructor and the number of students

import java.util.Scanner;
	public class UserInput{
		public static void main(String[] args) {
			Scanner myScanner = new Scanner( System.in );
			System.out.println("Please enter the course number");
			//check whether the coursenumber is vali
				while(myScanner.hasNextInt() == false)
			{
				System.out.println("please enter a valid course number ");
				myScanner.next();
			}
		  if(myScanner.hasNextInt()) {
				int coursenumber = myScanner.nextInt();
				System.out.println("your course number " + coursenumber);
			}
			System.out.println("Please enter the department name");
			while(myScanner.hasNext() == false){
				System.out.println("please enter a valid department name ");
				myScanner.next();
			}
			  if(myScanner.hasNext()) {
				String departmentname = myScanner.next();
					System.out.println("the department name is " + departmentname);		
			}
		System.out.println("Please enter the number of times it meets in a week");
			while(myScanner.hasNextInt() == false){
				System.out.println("please enter a valid  number of times ");
				myScanner.next();
			}
		
		  if(myScanner.hasNextInt()) {
				int numtime = myScanner.nextInt();
				System.out.println("the number of times it meets in a week " + numtime);		
			}
				System.out.println("Please enter the time the class starts");
				while(myScanner.hasNext() == false)
			{
				
				System.out.println("please enter a valid time the class starts ");
				myScanner.next();
			}
			
			  if(myScanner.hasNext()) {
				String time = myScanner.next();
					System.out.println("time the class starts " + time);			
			}
					System.out.println("Please enter instructor name");
			while(myScanner.hasNext() == false)
			{
					System.out.println("please enter a valid instructor name ");
				myScanner.next();
			}
		
			  if(myScanner.hasNext()) {
				String instructorname = myScanner.next();
					System.out.println(" instructor name " + instructorname );	
			}
					System.out.println("Please enter the number of students");
			while(myScanner.hasNextInt() == false)
			{
				System.out.println("please enter a valid  number of student ");
				myScanner.next();
			}
			
		  if(myScanner.hasNextInt()) {
				int numstudent = myScanner.nextInt();
				System.out.println("number of students " + numstudent);
				
			}
		}
	}