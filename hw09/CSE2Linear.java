import java.util.Scanner;
//cse2 
//practice searching algorithm
//xid220
//Xinyi Deng
public class CSE2Linear{
    public static void main(String[] args) {
   			    Scanner myScanner = new Scanner( System.in );
   			    System.out.println("Enter 15 ints for final grades in CSE2: ");
   			    int[] grade = new int[15];
			//use check to validation
                boolean check = false; 
                    for(int i=0;i<15;i++) { 
                        while( !check ){ 
                        if(myScanner.hasNextInt()) { 
                           grade[i] = myScanner.nextInt();
                                if(grade[i]>=0 && grade[i]<=100) { 
																	 if(i==0) {
                                    check=true;
                                    }
                                    if(i>0 && (grade[i]>=grade[i-1])) {
                                    check=true;
                                    }
                                }
                            if(grade[i]<0 || grade[i]>100) { 
                            System.out.print("Error: Enter int between 0-100: ");
                            }
                            if((i>0)&&(grade[i]<grade[i-1])) { 
                            System.out.print("Error: Enter int greater than previous input: ");
                            }
                        }
                        else{ 
                        System.out.print("Error: Enter an integer: ");
                        myScanner.next();
												}
												}
													
                        check=false;
                    }   
        System.out.println("");
        print(grade,"Enter 15 ascending ints for final grades in CSE2: ");
        System.out.print("Enter a grade to search for: ");
       //userinput
        int userNumber = myScanner.nextInt();  
        search(grade,userNumber);
        scramble(grade);
        System.out.println("");
        print(grade,"Scrambled:");
        System.out.print("Enter a grade to search for: ");
        userNumber = myScanner.nextInt();
			//call search function 
        search(grade,userNumber);
    }
    public static void search(int[] fgrade,int userNumber) { 
        int counter=0;
        boolean isFound=false;
        for(int i=0;i<15;i++){ 
        counter++; 
            if(fgrade[i] == userNumber){
            isFound=true;
            System.out.println(userNumber + " was found in the list with " + (i+1) + " iterations");
            break; 
            }
        }
        //if statement for if number was not found
        if(isFound == false){ 
        System.out.println(userNumber + " was not found in the list with " + counter + " iterations");
        }
    }
	//Print method	    
   	public static void print(int[] Grades,String one) { 
        String prints=one;
        for(int i=0;i<Grades.length;i++) { 
            prints+=(Grades[i]+" ");
        }
        System.out.println(prints); 
    }
    //Scramble method
    public static int[] scramble(int[] Grades){
        for (int i=0; i<Grades.length; i++){
            //Random number
	        int swap = (int)(Grades.length * Math.random());
	        int temp = Grades[swap];
	        Grades[swap] = Grades[i];
	        Grades[i] = temp;
        }
        return Grades;
    }
} 
                    