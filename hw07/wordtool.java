//hw7
//xid220
//Xinyi Deng
//cse002
//Objectives.  This homework has the objective of giving you practice writing methods, manipulating strings and forcing the user to enter good input. 
import java.util.Scanner; 
import java.util.regex.Pattern;
import java.util.regex.Matcher;
public class wordtool{

     public static void main(String []args){
     char user;
     // let user enter text
      System.out.println("Enter a sample text:");
      String entertext = "";
      Scanner keyboard = new Scanner(System.in);
      String line;
      while (keyboard.hasNextLine()) {
          line = keyboard.nextLine();
          if (line.isEmpty()) {
              break;
          }
          entertext += line + "\n";
      }
      sampleText(entertext);
      do {
      printMenu();
      Scanner scan1 = new Scanner(System.in);
      String in = scan1.next();
     while (in.length() != 1)
      {
         scan1 = new Scanner(System.in);
         in = scan1.next();
         System.out.println("you did not enter a character please enter again");
         //show main menu to user
          printMenu();
      }
     user = in.charAt(0);
    while((user != 'q') && (user != 'c') && (user != 'w') && (user != 'f') && (user != 'r') && (user != 's')){
        printMenu();
        System.out.println("This is not a valid character please enter again");
        Scanner scan2 = new Scanner(System.in);
       String in2= scan2.next();
       while (in2.length() != 1)
       {
         scan2 = new Scanner(System.in);
         in2= scan2.next();
         System.out.println("you did not enter a character please enter again");
          printMenu();
      }
        user = in2.charAt(0);
      }
    //quit the program while enter q
     if(user == 'q')
    {
       System.exit(0); 
    }
    else if(user == 'c')
    {
       int count1 =  getNumOfNonWSCharacters(entertext);
       System.out.println("Number of non-whitespace characters: " + count1);
    }
    else if(user == 'w')
    {
        int count2 = getNumOfWords(entertext);
         System.out.println("Number of words: " + count2);
    }
    else if (user == 'f')
    {   System.out.println(" Enter a word or phrase to be found: ");
         Scanner find1 = new Scanner(System.in);
        String find  = find1.next();
        findText(find, entertext);
    }
    else if(user == 'r')
    {
        String message = replaceExclamation(entertext);
        System.out.println ("Edited text:" + message);
    }
    else if(user == 's')
    {
        String shortmes = shortenSpace(entertext);
         System.out.println ("Edited text:" + shortmes);
    }
      }while(user != 'q');
     }
     
     public static void sampleText(String text)
     {
         System.out.println("You entered: " + text);
     }
     //This is the main menu display to user
     public static void printMenu()
     {
          System.out.println("MENU");
          System.out.println("c - Number of non-whitespace characters");
          System.out.println("w - Number of words");
          System.out.println("f - Find text");
          System.out.println("r - Replace all !'s");
          System.out.println("s - Shorten spaces");
          System.out.println("q - Quit");
          System.out.println(" ");
          System.out.println("Choose an option:");
     }
     // count number of character
       public static int getNumOfNonWSCharacters( String text) 
       {
    	   String text2 = text.replaceAll("\\s+","");
           return text2.length();
       }
       public static int getNumOfWords(String text)
       {
    	  String text2 = shortenSpace(text);
          
           int number = text2.length();
           int count = 0;
           for(int i = 0; i < number; i++)
           {
               if(text2.charAt(i) == ' ')
               {
                  count++;
               }
           }
           return count;
       }
       //find the number of times a strings occurs in text
       public static int findText(String find, String sample)
       {
           int times = 0;
           for (int i = 0; i <= sample.length() - find.length(); i++)
           {
               String substring = sample.substring(i, i+find.length());
               if(substring.equals(find))
               {
                  times = times + 1;
               }
           }
           System.out.println(find  + " instances: " + times);
           return times;
       }
       // replace ! to .
       public static String replaceExclamation( String text)
       {
    	   String replaceString=text.replace('!','.');
           return  replaceString;
       }
       
      public static String shortenSpace( String str)
      {
    	  StringBuilder sb=new StringBuilder();
          for(String s: str.split(" ")){

              if(!s.equals(""))        // ignore space
               sb.append(s+" ");       // add word with 1 space

          }
          return new String(sb.toString());
      }
       
       
}